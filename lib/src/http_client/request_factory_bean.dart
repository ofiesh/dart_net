part of http_client;

class RequestBeanFactory extends BeanFactory {
  final Type type;
  RequestBeanFactory(this.type);

  Object getObject() {
    _RequestProxy proxy = new _RequestProxy.newRequestProxy(type);
    return proxy;
  }
}