part of http_client;

class Request {
  final HttpRequestMethod method;
  final String url;
  const Request({this.method, this.url});
}