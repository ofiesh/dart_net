part of http_client;

class HttpRequestMethod {
  final _value;
  const HttpRequestMethod._internal(this._value);
  static const HttpRequestMethod GET = const HttpRequestMethod._internal("GET");
  static const HttpRequestMethod POST = const HttpRequestMethod._internal("POST");
  static const HttpRequestMethod PUT = const HttpRequestMethod._internal("PUT");
  static const HttpRequestMethod DELETE = const HttpRequestMethod._internal("DELETE");
  String toString() => _value;
}