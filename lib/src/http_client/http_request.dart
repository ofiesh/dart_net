part of http_client;

typedef HttpRequestCallback(String string);
class _HttpRequest {
  final Logger log = new Logger("_HttpRequest");
  final HttpRequestMethod _httpRequestMethod;
  final String _url;
  Object payload = null;
  HttpRequestCallback callback = null;
  bool async = true;
  _HttpRequest(this._httpRequestMethod, this._url);

  void execute() {
    log.fine("url: $_url");
    HttpRequest httpRequest = new HttpRequest();
    httpRequest.open(_httpRequestMethod.toString(), _url, async: async);
    if(payload != null)
      httpRequest.setRequestHeader("Content-type","application/json");
    httpRequest.onLoad.listen((event) {
      if(callback != null)
        callback(event.target.responseText);
    });
    if(payload != null &&
      (_httpRequestMethod == HttpRequestMethod.POST || _httpRequestMethod == HttpRequestMethod.PUT)) {
      log.fine(payload);
      httpRequest.send(payload);
    } else {
      httpRequest.send();
    }
  }
}