part of http_client;

class _RequestMethod {
  Symbol requestBodySymbol = const Symbol("requestBody");
  Symbol listSymbol = const Symbol("List");
  final Logger log = new Logger("_RequestMethod");
  final ClassMirror _classMirror;
  final Invocation _invocation;
  _RequestMethod(this._classMirror, this._invocation);

  execute(List<Object> positionalArgs, Map<Symbol, Object> namedArgs) {
    MethodMirror methodMirror = _classMirror.declarations[_invocation.memberName];
    Request request = null;
    for(InstanceMirror instanceMirror in methodMirror.metadata) {
      if(instanceMirror.reflectee is Request) {
        request = instanceMirror.reflectee;
        break;
      }
    }

    String url = request.url;
    for(int i = 0; i < methodMirror.parameters.length; i++) {
      ParameterMirror parameterMirror = methodMirror.parameters[i];

      try {
        if(parameterMirror.metadata != null && parameterMirror.metadata.length > 0
            && parameterMirror.metadata.first.reflectee is Param) {
          Param param = parameterMirror.metadata.first.reflectee;
          url = url.replaceFirst("\{${param.value}\}", _replaceArg(positionalArgs[i]));
        }
      } catch (unimplementedError) {
        //TODO when https://code.google.com/p/dart/issues/detail?id=15820
      }

      //TODO unneeded when https://code.google.com/p/dart/issues/detail?id=15820 is fixed
      if(parameterMirror.isNamed) {
        url = url.replaceFirst("\{${MirrorSystem.getName(parameterMirror.simpleName)}\}",
            _replaceArg(namedArgs[parameterMirror.simpleName]));
      }
    }
    log.info("${request.url} changed to $url");

    var returnObject;
    var doReturn = false;
    _HttpRequest httpRequest = new _HttpRequest(request.method, url);
    if(methodMirror.returnType.simpleName != const Symbol("void")
        && methodMirror.returnType.simpleName != const Symbol("dynamic")) {
      doReturn = true;
      httpRequest.async = false;

      httpRequest.callback = (String string) {
        Type type;
        TypeMirror typeMirror = methodMirror.returnType;
        if(typeMirror.simpleName == listSymbol) {
          type = (methodMirror.returnType.typeArguments.first as ClassMirror).reflectedType;
        } else {
          type = (typeMirror as ClassMirror).reflectedType;
        }
        returnObject = new JsonDeserializer(type).deserialize(string);
      };
    }

    if(namedArgs.containsKey(requestBodySymbol)) {
        String requestBody = new JsonSerializer().serialize(namedArgs[requestBodySymbol]);
        httpRequest.payload = requestBody;
    }

    httpRequest.execute();

    return returnObject;
  }
}

_replaceArg(Object arg) {
  String replacement = "";
  if(arg is List) {
    for(int j = 0; j < arg.length; j++) {
      if(j != 0)
        replacement += ",";
      replacement += arg[j].toString();
    }
  } else
    replacement = arg.toString();
  return replacement;
}