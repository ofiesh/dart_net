part of http_client;

@proxy
class _RequestProxy {
  static final Symbol _constructorSymbol = new Symbol("");
  final ClassMirror _classMirror;
  
  _RequestProxy._internal(this._classMirror);
  
  factory _RequestProxy.newRequestProxy(Type requestInterface) {
    ClassMirror classMirror = reflectClass(requestInterface);
    return new _RequestProxy._internal(classMirror);    
  }
  
  noSuchMethod(Invocation invocation) {
    _RequestMethod requestMethod = new _RequestMethod(_classMirror, invocation);
    return requestMethod.execute(invocation.positionalArguments, invocation.namedArguments);
  }
}