library http_client;

import "dart:html";
@MirrorsUsed(targets: "Request")
import "dart:mirrors";
import "package:json_serialization/json_serialization.dart";
import "package:logging/logging.dart";
import "package:updart/updart.dart";

part "src/http_client/http_callback_factory.dart";
part "src/http_client/http_client.dart";
part "src/http_client/http_request.dart";
part "src/http_client/param.dart";
part "src/http_client/request.dart";
part "src/http_client/request_factory_bean.dart";
part "src/http_client/request_method.dart";
part "src/http_client/http_request_method.dart";
part "src/http_client/request_proxy.dart";