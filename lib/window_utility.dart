library window_utility;

import "dart:html";

class WindowUtility {
  static Map<String, String> getUriParameters() {
    Map<String, String> paramMap = {};

    if(window.location.search != null && window.location.search.length > 0) {
      final List<String> paramValuePairs =
          //TODO Why substring 1?
          window.location.search.substring(1).split("&");

      paramValuePairs.forEach((String pair) {
        if (pair.contains("=")) {
          final paramValue = pair.split("=");
          paramMap[paramValue[0]] = paramValue[1];
        } else {
         paramMap[pair] = "";
        }
      });
    }

    return paramMap;
  }
}